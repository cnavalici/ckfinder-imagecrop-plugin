<?php
/**
    CKFinder Imagecrop Plugin
    Copyright (C) 2015 Cristian Năvălici

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

$config = new \stdClass;
$config->jquery_path = 'imgarea/jquery.min.js';
$config->jquery_imgarea_js = 'imgarea/jquery.imgareaselect.pack.js';
$config->jquery_imgarea_css = 'imgarea/imgareaselect-default.css';
$config->min_range = 0;
$config->max_range = 5000;
$config->initial_selection_x1 = 0;
$config->initial_selection_y1 = 0;
$config->initial_selection_x2 = 50;
$config->initial_selection_y2 = 50;
$config->min_width = 50;
$config->min_height = 50;
$config->jpeg_quality = 90;
$config->png_quality = 9;
$config->display_max_width = 700;
