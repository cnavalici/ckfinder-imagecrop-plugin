/**
    CKFinder Imagecrop Plugin
    Copyright (C) 2015 Cristian Năvălici

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

CKFinder.addPlugin('imagecrop', function(api) {
    CKFinder.dialog.add('cropDialog', function(api) {
        var fileurl =  '';

        var dialogDefinition =
        {
            inIframe : false,
            inPopup : true,
            minWidth : 800,
            minHeight : 500,
            onShow: function() {
                this.setTitle('ImageCrop - ' + api.getSelectedFile().name);
                fileurl = api.getSelectedFile().getUrl();

                var dialog = this;
                doc = dialog.getElement().getDocument();
                var selfile = api.getSelectedFile();

                doc.getById('ifcrop').setAttribute('src',
                    CKFinder.getPluginPath('imagecrop') +
                    'cropdialog.php?fileurl=' + CKFinder.config.baseUrl + api.getSelectedFile().getUrl()
                    + '&filename=' + selfile.name
                    + '&foldername=' + selfile.folder.name
                    + '&folderpath=' + api.getSelectedFolder().getUrl());

                return true;
            },
            onCancel: function() {
                api.refreshOpenedFolder();
                return true;
            },
            contents : [
                {
                    id : 'cropDialogId',
                    label : 'label',
                    expand: true,
                    padding: 0,
                    elements : [
                        {
                            type : 'html',
                            id: 'iframe_crop',
                            expand : true,
                            padding : 0,
                            html: '<iframe id="ifcrop" style="width:100%; height: 100%;"></iframe>'
                        }
                    ]
                }
            ],
            //buttons : [ CKFinder.dialog.okButton, CKFinder.dialog.cancelButton ],
            buttons: []
        }

        return dialogDefinition;
    });




    api.addFileContextMenuOption(
        { label : 'Image Crop', command : 'ImageCrop' } ,
        function(api, file) {
            if (!file.isImage()) {
                api.openMsgDialog("Image cropping", "This feature is only available for cropping images.");
            } else {
                api.openDialog('cropDialog');
            }
        },
        function (file) {
            // Disable for files other than images.
            if ( !file.isImage() || !api.getSelectedFolder().type )
                return false;
            if ( file.folder.acl.fileDelete && file.folder.acl.fileUpload )
                return true;
            else
                return -1;
        }
    );

});
