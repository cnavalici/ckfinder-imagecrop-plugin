<?php
/**
    CKFinder Imagecrop Plugin
    Copyright (C) 2015 Cristian Năvălici

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

require_once './config.php';
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset='UTF-8'>
        <script src="<?php echo $config->jquery_path ?>" type="text/javascript"></script>
        <script src="<?php echo $config->jquery_imgarea_js ?>" type="text/javascript"></script>
        <link rel="stylesheet" href="<?php echo $config->jquery_imgarea_css ?>" type="text/css" />

        <style>
            body { margin: 0; padding: 0; }

            .ic_msg {
                font-size: 0.7em;
            }
        </style>
    </head>
    <body>
        <div>
            <label for="output_width">Width</label><input type="text" name="output_width" id="owidth" value="50" size="4"/>
            <label for="output_height">Height</label><input type="text" name="output_height" id="oheight" value="50" size="4"/>
            <input type="button" value="Set" id="btn_aspect_ratio"><span id="msg_aspect_ratio" class="ic_msg"></span>
        </div>

        <div class="ic_crop">
            <img src="<?php echo filter_var($_GET['fileurl'], FILTER_VALIDATE_URL); ?>" id="img_crop"
                style="max-width: <?php echo $config->display_max_width; ?>px">
        </div>

        <div>
            <input type="submit" name="submit" value="Submit" id="btn_submit" />
            <span id="msg_submit" class="ic_msg"></span>
        </div>

        <input type="hidden" value="<?php echo filter_var($_GET['filename'], FILTER_SANITIZE_STRING); ?>" id="filename">
        <input type="hidden" value="<?php echo filter_var($_GET['foldername'], FILTER_SANITIZE_STRING); ?>" id="foldername">
        <input type="hidden" value="<?php echo filter_var($_GET['folderpath'], FILTER_SANITIZE_STRING); ?>" id="folderpath">

        <script>
            var display_max = <?php echo $config->display_max_width; ?>;

            $(document).ready(function () {
                var x1 = 0, x2 = 0, y1 = 0, y2 = 0;
                var height = 0, width = 0;

                var realImageWidth = 0;
                var coef = 1.00;
                $.get('crop.php', { "fileurl": $("#img_crop").attr('src'), "getrealwidth": 1 }, function(resp) {
                    realImageWidth = resp;
                    if (realImageWidth > display_max) {
                        coef = (realImageWidth/display_max).toFixed(4);
                    }
                });

                var min_height = <?php echo $config->min_height ?>;
                var min_width = <?php echo $config->min_width ?>;

                // setting aspect ratio means setting output size
                $("#btn_aspect_ratio").click(function() {
                    var swidth = Math.round(check_min_value($("#owidth"), min_width) / coef);
                    var sheight = Math.round(check_min_value($("#oheight"), min_height) / coef);

                    $('#img_crop').imgAreaSelect({
                        x1: 0,
                        y1: 0,
                        x2: swidth,
                        y2: sheight,

                        handles: true,
                        aspectRatio: swidth + ':' + sheight,
                        minHeigth: sheight,
                        minWidth: sheight,
                        persistent: true,
                        onSelectEnd: function(img, selection) {
                            x1 = selection.x1;
                            x2 = selection.x2;
                            y1 = selection.y1;
                            y2 = selection.y2;

                            height = (y2 - y1) * coef;
                            width = (x2 - x1) * coef;
                        }
                    });
                });

                $("#btn_submit").click(function() {
                     if ((height < min_height) || (width < min_width)) {
                        $("#msg_aspect_ratio").html("Your selection must be at least " + min_width + " X " + min_height);
                        return false;
                    }
                    $.post('crop.php', {
                            "x1": Math.round(x1 * coef),
                            "x2": Math.round(x2 * coef),
                            "y1": Math.round(y1 * coef),
                            "y2": Math.round(y2 * coef),
                            "ow": check_min_value($("#owidth")),
                            "oh": check_min_value($("#oheight")),
                            "fileurl": $("#img_crop").attr('src'), "folderpath": $("#folderpath").val() },
                            function(resp) {
                                $("#msg_submit").html(resp);
                                $(".cke_dialog_close_button", window.parent.document).click();
                            }
                    );
                });

                var check_min_value = function(elem, min_value) {
                    if(typeof(String.prototype.trim) != 'function') {
                        String.prototype.trim = function() {
                            return this.replace(/^\s+|\s+$/g, '');
                        }
        		    }

                    var value = elem.val().trim();
                    if (value < min_value) {
                        elem.val(min_value);
                    }
                    return value;
                }
            });
        </script>
    </body>
</html>
