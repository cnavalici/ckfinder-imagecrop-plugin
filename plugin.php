<?php
/**
    CKFinder Imagecrop Plugin
    Copyright (C) 2015 Cristian Năvălici

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

if (!defined('IN_CKFINDER')) exit;


/**
 * Include base XML command handler
 */
require_once CKFINDER_CONNECTOR_LIB_DIR . "/CommandHandler/XmlCommandHandlerBase.php";

class CKFinder_Connector_CommandHandler_ImageCrop extends CKFinder_Connector_CommandHandler_XmlCommandHandlerBase {
    public function onBeforeExecuteCommand(&$command) {
        if ($command == 'ImageCrop') {
            $this->sendResponse();
            return false;
        }

        return true ;
    }

    public function buildXml() {
        // basic checkings
        $this->checkConnector();
        $this->checkRequest();

        // checking permissions
        if (!$this->_currentFolder->checkAcl(CKFINDER_CONNECTOR_ACL_FILE_VIEW)) {
            $this->_errorHandler->throwError(CKFINDER_CONNECTOR_ERROR_UNAUTHORIZED);
        }

        // Make sure we actually received a file name
        if (!isset($_GET["fileName"])) {
            $this->_errorHandler->throwError(CKFINDER_CONNECTOR_ERROR_INVALID_NAME);
        }

        $fileName = CKFinder_Connector_Utils_FileSystem::convertToFilesystemEncoding($_GET["fileName"]);
        $resourceTypeInfo = $this->_currentFolder->getResourceTypeConfig();

        // Use the resource type configuration object to check whether the extension of a file to check is really allowed.
        if (!$resourceTypeInfo->checkExtension($fileName)) {
            $this->_errorHandler->throwError(CKFINDER_CONNECTOR_ERROR_INVALID_EXTENSION);
        }

        // Make sure that the file name is really ok and has not been sent by a hacker
        if (!CKFinder_Connector_Utils_FileSystem::checkFileName($fileName) || $resourceTypeInfo->checkIsHiddenFile($fileName)) {
            $this->_errorHandler->throwError(CKFINDER_CONNECTOR_ERROR_INVALID_REQUEST);
        }

        $filePath = CKFinder_Connector_Utils_FileSystem::combinePaths($this->_currentFolder->getServerPath(), $fileName);

        if (!file_exists($filePath) || !is_file($filePath)) {
            $this->_errorHandler->throwError(CKFINDER_CONNECTOR_ERROR_FILE_NOT_FOUND);
        }

        $oNode = new Ckfinder_Connector_Utils_XmlNode("ImageCrop");
        $oNode->addAttribute("NewName", $new_name);
        $this->_connectorNode->addChild($oNode);
    }


    public function onInitCommand(&$connectorNode)
    {
        $imageresize = new CKFinder_Connector_Utils_XmlNode("imagecrop");
        //$imageresize->addAttribute('CKFBaseUrl', $config->media_folder);

        // "@" protects against E_STRICT (Only variables should be assigned by reference)
        @$pluginsInfo = &$connectorNode->getChild("PluginsInfo");
        $pluginsInfo->addChild($imageresize);

        return true ;
    }
}


$CommandHandler_ImageCrop = new CKFinder_Connector_CommandHandler_ImageCrop();
$config['Hooks']['BeforeExecuteCommand'][] = array($CommandHandler_ImageCrop, "onBeforeExecuteCommand");
$config['Hooks']['InitCommand'][] = array($CommandHandler_ImageCrop, "onInitCommand");
$config['Plugins'][] = 'imagecrop';
