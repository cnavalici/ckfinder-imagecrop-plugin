<?php
/**
    CKFinder Imagecrop Plugin
    Copyright (C) 2015 Cristian Năvălici

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

// error_reporting(E_ALL);
require_once './config.php';

session_start();

class ImageOp {
    public $width;
    public $height;
    public $source;

    public $start_x;
    public $start_y;
    public $destination;

    public $owidth;
    public $oheight;

    protected $config;

    private $ext;

    public function __construct($config) {
        $this->config = $config;
    }

    public function do_cropping() {
        $this->ext = $this->extract_extension($this->source);

        $new_img = imagecreatetruecolor($this->owidth, $this->oheight);
        $old_img = $this->imagecreatefromfile($this->source);

        imagecopyresampled( $new_img,
                            $old_img,
                            0, 0,
                            $this->start_x, $this->start_y,
                            $this->owidth, $this->oheight,
                            $this->width, $this->height);

        return $this->imagefile($new_img, $this->destination);
    }

    private function imagecreatefromfile($source) {
        switch ($this->ext) {
            case 'jpeg':
            case 'jpg':
                return imagecreatefromjpeg($source);
            break;

            case 'png':
                return imagecreatefrompng($source);
            break;

            case 'gif':
                return imagecreatefromgif($source);
            break;

            default:
                throw new \InvalidArgumentException('File "'.$source.'" is not valid jpg, png or gif image.');
            break;
        }
    }


    private function imagefile($new_img, $new_filename) {
        $args = array($new_img, $new_filename);
        switch ($this->ext) {
            case 'jpeg':
            case 'jpg':
                $func = "imagejpeg";
                $args[] = $this->config->jpeg_quality;
            break;

            case 'png':
                $func = "imagepng";
                $args[] = $this->config->png_quality;
            break;

            case 'gif':
                $func = "imagegif";
            break;

            default:
                throw new \InvalidArgumentException('File "'.$new_filename.'" is not valid jpg, png or gif image.');
            break;
        }

        return call_user_func_array($func, $args);
    }


    private function extract_extension($filename) {
        $parts = pathinfo($filename);
        return $parts['extension'];
    }
}



class ImageCrop {
    private static $cropped_ext = '_cropped_';
    private $imageop = null;

    protected $fileurl;
    protected $folderpath;

    public $x1, $x2, $y1, $y2;
    public $owidth, $oheight;
    public $new_filename;

    protected $config;

    public function __construct($config) {
        $this->imageop = new ImageOp($config);
        $this->config = $config;
    }


    public function set_fileurl($fileurl_raw) {
        $this->fileurl = filter_var($fileurl_raw, FILTER_VALIDATE_URL, FILTER_FLAG_PATH_REQUIRED);
    }

    public function set_folderpath($folderpath_raw) {
        $this->folderpath = filter_var($folderpath_raw, FILTER_SANITIZE_STRING);
    }

    private function calculate_dimensions() {
        return array('height' => $this->y2 - $this->y1, 'width' => $this->x2 - $this->x1);
    }


    public function __set($dim, $value) {
        $coordinates = array('x1', 'x2', 'y1', 'y2');

        if (in_array($dim, $coordinates)) {
            if (property_exists($this, $dim)) {
                $filter = filter_var($dim, FILTER_VALIDATE_INT, array("options" =>
                    array("min_range" => $this->config->min_range, "max_range" => $this->config->max_range)
                ));
                $this->$c = ($filter) ? $filter : 0;
            }
        }

        return $this;
    }


    public function crop() {
        $dim = $this->calculate_dimensions();

        $this->imageop->width = $dim['width'];
        $this->imageop->height = $dim['height'];
        $this->imageop->source = $this->fileurl;
        $this->imageop->start_x = $this->x1;
        $this->imageop->start_y = $this->y1;
        $this->imageop->owidth = $this->owidth;
        $this->imageop->oheight = $this->oheight;

        $this->imageop->destination = $_SERVER['DOCUMENT_ROOT'] . $this->folderpath. $this->generate_new_name();

        return $this->imageop->do_cropping();
    }

    public function get_real_width() {
        list($width, $height, $type, $attr) = getimagesize($this->fileurl);

        return $width;
    }

    private function generate_new_name() {
        $parts = pathinfo($this->fileurl);

        $current_filename = $parts['filename'];
        $extension = $parts['extension'];

        $index_already_cropped = stripos($current_filename, self::$cropped_ext);

        if ($index_already_cropped !== FALSE) {
            $base_filename = substr($current_filename, 0, $index_already_cropped);
        } else {
            $base_filename = $current_filename;
        }

        $index = 1;
        $new_filename = $this->assemble_filename($base_filename, $index, $extension);
        while(file_exists($_SERVER['DOCUMENT_ROOT'] . $this->folderpath . $new_filename)) {
            $index++;
            $new_filename = $this->assemble_filename($base_filename, $index, $extension);
        }

        $this->new_filename = $new_filename;
        return $new_filename;
    }


    private function assemble_filename($base, $index, $extension) {
        return sprintf("%s%s%03s.%s", $base, self::$cropped_ext, $index, $extension);
    }
}



if (isset($_POST['fileurl'])) {
    $img = new ImageCrop($config);

    $img->x1 = (int)$_POST['x1'];
    $img->x2 = (int)$_POST['x2'];
    $img->y1 = (int)$_POST['y1'];
    $img->y2 = (int)$_POST['y2'];

    $img->owidth= (int)$_POST['ow'];
    $img->oheight = (int)$_POST['oh'];

    $img->set_fileurl($_POST['fileurl']);
    $img->set_folderpath($_POST['folderpath']);

    $res = $img->crop();
    if ($res) {
        printf("The new image was saved as %s", $img->new_filename);
    } else {
        printf("Unable to save the cropped image.");
    }
}

if (isset($_GET['fileurl'])) {
    if ($_GET['getrealwidth']) {
        $img = new ImageCrop($config);
        $img->set_fileurl($_GET['fileurl']);

        echo $img->get_real_width();
    }
}
