Image Cropping Plugin for CKFinder
=========================

CkFinder ImageCrop Plugin is based on jquery and jquery imgarea, and it's doing two operations at once:
- cropping the image
- resizing the crop to match the desired output

### How to install

Assuming that you already install [CKFinder](http://cksource.com/ckfinder):

##### Download the package

Download either the [zip file](https://github.com/cristianav/ckfinder-imagecrop-plugin/archive/master.zip) or clone the [git repository](https://github.com/cristianav/ckfinder-imagecrop-plugin) into your ckfinder plugins location (usually ckfinder/plugins), into a new folder **imagecrop**.

Example:

    ls /var/www/ckfinder/plugins
    dummy  fileeditor  flashupload  gallery  imagecrop  imageresize  watermark  zip

##### Update CKFinder config

Update **ckfinder/config.js** with your real baseURL:

    CKFinder.config.baseUrl = 'http://localhost';

In **ckfinder/config.php** update the last part where the plugins are loaded:

    ...
    include_once "plugins/zip/plugin.php";
    include_once "plugins/imagecrop/plugin.php"; // this is the imagecrop loader

    $config['plugin_imageresize']['smallThumb'] = '90x90';
    ...

##### Use it

Right click on any image from your folder it will show a new option: Image Crop.


### How to use it

Three simple steps (for your new popup window)

1. Setup the desired output image size (in pixels). Click on _Set_ button.
2. Move the image select area to the desired position; increase as necessary if you need to capture more from the original image.
3. Submit. A new image will be created based on your selection and the output size. The new filename will include _cropped_ and some numbering.

### Licence
Released under GPLv3. Check LICENSE file for more details.

### Credits

Cristian NĂVĂLICI
ncristian@lemonsoftware.eu
